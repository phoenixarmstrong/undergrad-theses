#!/bin/bash
#SBATCH --nodes=2
#SBATCH --time=7-00:00      # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --mail-type=ALL
#SBATCH --mail-user=shanewidanagama@trentu.ca

module load StdEnv/2020 pilon/1.23

#polisheds merged assembly (quickmerge step 2)
java -Xmx256G -jar $EBROOTPILON/pilon.jar --genome ../../Quickmerge_Assemblies/t8_canu+merged/merged_canu+dbg2olc.fasta --bam mapping.sorted.bam --output t_latifolia_polished
