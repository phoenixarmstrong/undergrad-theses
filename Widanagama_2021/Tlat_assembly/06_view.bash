#!/bin/bash
#SBATCH --time=0-16:00
#SBATCH --account=rrg-shaferab
#SBATCH --mem=8G
module load samtools
samtools view m54204U_200925_191208.subreads.bam > tl_subreads.sam
