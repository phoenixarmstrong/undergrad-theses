#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=abss_ctg
#SBATCH --cpus-per-task=32
#SBATCH --mem=125G
#SBATCH --time=00-48:00
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
# time (DD-HH:MM)

module load gcc/5.4.0
module load abyss/2.2.4

#run paired-end abyss with a kmer length 101 using 100% of trimmed PE Illumina reads to generate contigs 
abyss-pe name=tl k=101 in='3LM2_1_D_S19_L005_trim_R1_001.fastq 3LM2_1_D_S19_L005_trim_R2_001.fastq'
