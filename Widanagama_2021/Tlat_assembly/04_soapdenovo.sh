#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=SOAPdenovo2-all
#SBATCH --cpus-per-task=16
#SBATCH --mem=400G
#SBATCH --time=0-23:00
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL


module load soapdenovo2/r240 #load module

#run all SOAPdenovo2 steps using the default config file, a kmer length of 61, and output results to the specified directory
SOAPdenovo-63mer all -s configFile -o sd3libsdefault/output61 -K 61
