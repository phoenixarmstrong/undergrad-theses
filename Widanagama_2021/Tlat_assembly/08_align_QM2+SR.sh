#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bwa-bam-sort
#SBATCH --cpus-per-task=16
#SBATCH --mem=48G
#SBATCH --time=0-48:00
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
module load bwa
module load samtools

#align merged assembly (quickmerge step 2) to trimmed Illumina PE reads for polishing with 16 threads
bwa mem -t 16 ../../Quickmerge_Assemblies/t8_canu+merged/merged_canu+dbg2olc.fasta ../../archive/reads/3LM2_1_D_S19_L005_trim_R1_001.fastq ../../archive/reads/3LM2_1_D_S19_L005_trim_R2_001.fastq | samtools view - -Sb | samtools sort - -@16 -o illumina_mapping/mapping.sorted.bam
#index alignment
samtools index illumina_mapping/mapping.sorted.bam 
