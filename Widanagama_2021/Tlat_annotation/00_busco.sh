#!/bin/bash
#SBATCH --mem=8G
#SBATCH --time=6-00:00      # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --mail-type=ALL
#SBATCH --mail-user=shanewidanagama@trentu.ca

module load nixpkgs/16.09 gcc/7.3.0 openmpi/3.1.4 busco/3.0.2

#export paths to BUSCO config
export BUSCO_CONFIG_FILE=$HOME/busco_config.ini

#identify proportion of BUSCOs in merged genome assembly (Quickmerge step 2) with Liliopsida lineage, in genome mode, and force the rewrite of existing files
run_BUSCO.py --in ../../Quickmerge_Assemblies/t8_canu+merged/merged_canu_dbg2olc.fasta --out BUSCO_TL --lineage_path ../liliopsida_odb10 --mode genome -f

