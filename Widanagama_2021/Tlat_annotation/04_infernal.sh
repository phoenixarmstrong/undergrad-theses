#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=infernalCmscan
#SBATCH --cpus-per-task=16
#SBATCH --mem=50G
#SBATCH --time=0-12:00
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
# time (DD-HH:MM)

#load required modules
module load infernal/1.1.3

#assemble the paired end short reads using 32 threads, and a 368 GB memory limit for making the kmer distribution
cmscan -Z 575.602 --cut_ga --rfam --nohmmonly --tblout tlat-genome.tblout --fmt 2 --clanin Rfam.clanin Rfam.cm ../../Pilon/t1/t_latifolia_polished.fasta > tlat-genome.cmscan

#these commands were used to find snRNA and miRNA counts from the output
#grep "snRNA" ../../../tlatifolia/annotation/infernal/tlat-genome.deoverlapped.tblout | wc
#grep "miRNA" ../../../tlatifolia/annotation/infernal/tlat-genome.deoverlapped.tblout | wc
#grep "microRNA" ../../../tlatifolia/annotation/infernal/tlat-genome.deoverlapped.tblout | wc
