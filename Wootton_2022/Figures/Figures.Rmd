---
title: "Figures"
output:
  html_document:
    toc: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r echo=FALSE}
setwd("~/Shafer Lab")
library(ggplot2)

df <- read.csv("MG_Genome_Health_Data.csv")
```


***

%ROH: PLINK (100KB) <br>
F-Statistic: PLINK (LD-pruned) <br>
Homozygous Deleterious Count: VEP/SIFT (MAF = 5%) <br>

***

## Regressions

***

### Latitude vs %ROH 
<br>
```{r, echo = FALSE, message = FALSE}
setwd("~/Shafer Lab")
library(ggplot2)

df <- read.csv("MG_Genome_Health_Data.csv")

ggplot(df, aes(x=Latitude, y=X100KB)) +
  geom_point() +
  theme_classic() +
  geom_smooth(method = 'lm', colour = "red") +
  labs(x="Latitude", y="%ROH")
```

***

### Latitude vs F 
<br>
```{r, echo = FALSE, message = FALSE}
setwd("~/Shafer Lab")
library(ggplot2)

df <- read.csv("MG_Genome_Health_Data.csv")

ggplot(df, aes(x=Latitude, y=plink_het_prune)) +
  geom_point() +
  theme_classic() +
  geom_smooth(method = 'lm', colour = "red") +
  labs(x="Latitude", y="F")
```

***

### Latitude vs Homozygous Deleterious Count 
<br>
```{r, echo = FALSE, message = FALSE}
setwd("~/Shafer Lab")
library(ggplot2)

df <- read.csv("MG_Genome_Health_Data.csv")

ggplot(df, aes(x=Latitude, y=X._homozygous_deleterious)) +
  geom_point() +
  theme_classic() +
  geom_smooth(method = 'lm', colour = "red") +
  labs(x="Latitude", y="Homozygous Deleterious Count")
```

***

### Longitude vs %ROH 
<br>
```{r, echo = FALSE, message = FALSE}
setwd("~/Shafer Lab")
library(ggplot2)

df <- read.csv("MG_Genome_Health_Data.csv")

ggplot(df, aes(x=Longitude, y=X100KB)) +
  geom_point() +
  theme_classic() +
  geom_smooth(method = 'lm', colour = "red") +
  labs(x="Longitude", y="%ROH")
```

***

### Longitude vs F 
<br>
```{r, echo = FALSE, message = FALSE}
setwd("~/Shafer Lab")
library(ggplot2)

df <- read.csv("MG_Genome_Health_Data.csv")

ggplot(df, aes(x=Longitude, y=plink_het_prune)) +
  geom_point() +
  theme_classic() +
  geom_smooth(method = 'lm', colour = "red") +
  labs(x="Longitude", y="F")
```

***

### Longitude vs Homozygous Deleterious Count 
<br>
```{r, echo = FALSE, message = FALSE}
setwd("~/Shafer Lab")
library(ggplot2)

df <- read.csv("MG_Genome_Health_Data.csv")

ggplot(df, aes(x=Longitude, y=X._homozygous_deleterious)) +
  geom_point() +
  theme_classic() +
  geom_smooth(method = 'lm', colour = "red") +
  labs(x="Longitude", y="Homozygous Deleterious Count")
```

***

### %ROH vs F 
<br>
```{r, echo = FALSE, message = FALSE}
setwd("~/Shafer Lab")
library(ggplot2)

df <- read.csv("MG_Genome_Health_Data.csv")

ggplot(df, aes(x=X100KB, y=plink_het_prune)) +
  geom_point() +
  theme_classic() +
  geom_smooth(method = 'lm', colour = "red") +
  labs(x="%ROH", y="F")
```

***

### %ROH vs Homozygous Deleterious Count
<br>
```{r, echo = FALSE, message = FALSE}
setwd("~/Shafer Lab")
library(ggplot2)

df <- read.csv("MG_Genome_Health_Data.csv")

ggplot(df, aes(x=X100KB, y=X._homozygous_deleterious)) +
  geom_point() +
  theme_classic() +
  geom_smooth(method = 'lm', colour = "red") +
  labs(x="%ROH", y="Homozygous Deleterious Count")
```

***

### F vs Homozygous Deleterious Count
<br>
```{r, echo = FALSE, message = FALSE}
setwd("~/Shafer Lab")
library(ggplot2)

df <- read.csv("MG_Genome_Health_Data.csv")

ggplot(df, aes(x=plink_het_prune, y=X._homozygous_deleterious)) +
  geom_point() +
  theme_classic() +
  geom_smooth(method = 'lm', colour = "red") +
  labs(x="F", y="Homozygous Deleterious Count")
```

***

## Frequency Distributions

***

### %ROH
```{r, echo = FALSE, message = FALSE}
par(mfrow=c(2,1)) 

hist(df$X100KB,ylab="Frequency",xlab="%ROH",breaks=20,xlim=c(0,25),main=" ")

boxplot(df$X100KB,horizontal=TRUE,ylim=c(0,25),xlab="%ROH")

```

***

### F
```{r, echo = FALSE, message = FALSE}
par(mfrow=c(2,1)) 

hist(df$plink_het_prune,ylab="Frequency",xlab="F",breaks=20,xlim=c(-0.3,0.3),main=" ")

boxplot(df$plink_het_prune,horizontal=TRUE,ylim=c(-0.3,0.3),xlab="F")
```

***

### Deleterious Homozygous Count
```{r, echo = FALSE, message = FALSE}
par(mfrow=c(2,1)) 

hist(df$X._homozygous_deleterious,ylab="Frequency",xlab="Deleterious Homozygous Count",
     breaks=20,main=" ",xlim=c(5250,6250))

boxplot(df$X._homozygous_deleterious,ylim=c(5250,6250),
        horizontal = TRUE,xlab="Deleterious Homozygous Count")
```
