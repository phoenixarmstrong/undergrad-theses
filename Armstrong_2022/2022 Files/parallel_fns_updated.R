#
#  peaksim: 
#
#  Creates individual DNA profiles for NOC individuals,
#  and then mixes those profiles. 
#
#  Returns:
#    mix_df = mixture,
#    non_mix = peaksimD,
#    refs = refs,
#    mix = list(Sample = mix),
#    mx = mx,
#    rho = rho,
#    tau = tau,
#    mu = mu,
#    sigma = sigma
#
peaksim <- function(NOC,
                    popFreq, # population allele freq table
                    mx, # \theta_i from Cowell et al. - contribution proportion, DNA total
                    mu,
                    sigma) {
  locs <- names(popFreq)
  
  peaksimD <- data.frame(Marker = rep(locs, NOC),
                         Contributor = rep(1:NOC, each = length(popFreq)))
  allele_matrix <-
    matrix(data = NA,
           nrow = NOC * length(locs),
           ncol = 4)
  allele_matrix <- as.data.frame(allele_matrix)
  names(allele_matrix) <-
    paste0(c("Allele", "Height"), rep(1:NOC, each = 2))
  
  peaksimD <- data.frame(peaksimD, allele_matrix)
  
  # this is $\gamma \rho_m$ - fixed for all loci for now
  rho <- 1 / (sigma ^ 2)
  tau <- mu / rho

  for (n in 1:NOC) {
    # loop on contributors, for each ...
    
    for (loc in locs) {

      # work through markers
      # draw 2 allele expressions at random for each marker, for this individual,
      # from the popFreq array
      asamp <- sample(
        names(popFreq[[loc]]),
        size = 2,
        prob = popFreq[[loc]],
        replace = TRUE
      )
      
      #  Simulate W_{ia}^m : contribution of individual i (n here) to the
      #  peak height at allele a (not a variable) of marker m (loc here)
      #
      #  * note: does this twice, once for each allele - if homozyg, then
      #    addition applied since \eta_m is common
      #
      #  In Cowell, Lauritzen and Mortera:
      #    W_{ia}^m \sim \Gamma( \rho_m \gamma \theta_i n_{ia}^m,  \eta_m )
      #
      hsamp <- rgamma(length(asamp), # sampling as in EFM
                      shape = rho * mx[n],  # rho = \rho_M * \gamma * 1; mx[n] = \theta_i
                      scale = tau)          # tau = \eta_m; mu / rho - not super important
      
      # NOTE: in Cowell (and hence, EFM?), the tau term drops out in the likelihood anyway
      # Additional note: the common tau term is all that allows for summation -
      #   this actually means tau should be fixed for all subjects, otherwise
      #   this doesn't actually work. At least, as we've implemented it - and as
      #   it is assumed in Cowell et al.
      
      # asamp + hsamp - allele expressions and contributions to peak height
      
      peaksimD[peaksimD$Marker == loc & peaksimD$Contributor == n,
               c("Allele1", "Allele2", "Height1", "Height2")] <-
        c(asamp, round(hsamp, 2))
      
    }
  }
  peaksimD$Height1 <- as.numeric(peaksimD$Height1)
  peaksimD$Height2 <- as.numeric(peaksimD$Height2)
  
  #  now have simulated allele expressions at each marker for each
  #  individual, n = 1, 2, ..., NOC, as well as simulated RFUs for
  #  the same. They have not been combined yet.
  
  
  #
  #  Make mixture - drop contributor information, merge duplicate allele expressions
  #  at common markers
  #
  allele_matrix <-
    matrix(data = NA,
           nrow = length(locs),
           ncol = 4 * NOC)
  allele_matrix <- as.data.frame(allele_matrix)
  names(allele_matrix) <-
    paste0(c("Allele", "Height"), rep(1:(2 * NOC), each = 2))
  mixture <- data.frame(Marker = locs, allele_matrix)
  
  for (loc in locs) {
    tmp <- peaksimD[peaksimD$Marker == loc,]
    alleles <- c(tmp$Allele1, tmp$Allele2)
    heights <- c(tmp$Height1, tmp$Height2)
    uniq_alleles <- uniq_heights <- unique(alleles)
    for (j in 1:length(uniq_alleles)) {
      uniq_heights[j] <- sum(heights[alleles == uniq_alleles[j]])
    }
    mixture[mixture$Marker == loc,
            seq(from = 2,
                to = 2 * length(uniq_alleles),
                by = 2)] <- sort(uniq_alleles)
    mixture[mixture$Marker == loc,
            seq(
              from = 3,
              to = 2 * length(uniq_alleles) + 1,
              by = 2
            )] <- uniq_heights[order(uniq_alleles)]
  }
  
  # refs is a list of length NOC,
  # each of which is a list of length length(locs),
  # each of which is a list of adata - the alleles
  refs <- vector("list", length = NOC)
  for (n in 1:NOC) {
    refs[[n]] <- vector("list", length(locs))
    names(refs[[n]]) <- locs
    for (loc in locs) {
      dat <-
        as.character(peaksimD[peaksimD$Marker == loc &
                                peaksimD$Contributor == n,
                              seq(from = 3,
                                  to = ncol(peaksimD),
                                  by = 2)])
      
      refs[[n]][[loc]] <- list(adata = dat)
    }
  }
  names(refs) <- paste0("Contributor ", 1:NOC)
  
  mix <- vector("list", length = length(locs))
  names(mix) <- locs
  for (loc in locs) {
    dat1 <- mixture[mixture$Marker == loc,
                    seq(from = 2,
                        to = ncol(mixture),
                        by = 2)]
    names(dat1) <- NULL
    dat2 <- mixture[mixture$Marker == loc,
                    seq(from = 3,
                        to = ncol(mixture),
                        by = 2)]
    which_nNA <- which(!is.na(as.numeric(unlist(dat2))))
    mix[[loc]] <- list(adata = unlist(dat1)[which_nNA],
                       hdata = as.numeric(unlist(dat2))[which_nNA])
  }
  
  return(list(
    mix_df = mixture,
    non_mix = peaksimD,
    refs = refs,
    mix = list(Sample = mix),
    theta = list(
      mx = mx,
      rho = rho,
      tau = tau,
      mu = mu,
      sigma = sigma
    )
  ))
}

#
#  simplerun: 
#
#  Takes samples generated by peaksim, and, given conditions,
#  passes these samples into EuroForMix, and does the usual analysis
#  pipeline. Returns stripped down information about the analysis.
#
#  Returns:
#    modelTheta - thetahp, thetahd
#    LRs - upperlimit, MLE, conservative MCMC, integrated Laplace, marginal, marker
#    validModel - flags, sig-HP, sig-HD, acceptrates
#    DCrank
#
simplerun <- function(samples = profile$mix,  # from peaksim
                      refs = profile$refs,     # also from peaksim
                      PopFreq,  # population standard
                      NOC,      # NOC, should align with peaksim's input
                      kit = NULL,
                      POIind = 2,
                      #index, second reference is the person of interest (only H_p)
                      modelDegrad = TRUE,
                      modelBWstutt = TRUE,
                      modelFWstutt = TRUE,
                      findOptimalModel = TRUE, # find optimal model may be set to FALSE if above three arguments are set manually
                      fst = 0,
                      #coancestry coefficient, may be a vector
                      lambda = 0.01,
                      #drop-in model parameter, may be a vector
                      prC = 0.05,
                      #drop-in probability, may be a vector
                      threshT = 50,
                      #analytical threshold, may be a vector
                      alpha = 0.01,
                      #significance level used for the envelope test (validMLEmodel)
                      minF = NULL,
                      #minimum frequency used in the analysis (can be specified, otherwise minimum observed is used by default)
                      seed = 1,
                      #for contLikMLE/contLikMCMC
                      nDone = 2,
                      #optimizations in contLikMLE
                      mcmcIter = 1000, # 5000 iterations is ideal but requires more computing power
                      #number of iterations
                      mcmcDelta = 2,
                      #scaling of variance used for proposal in contLikMCMC (calibrated based on mcmc$accrat object)
                      intRelTol = 0.1,
                      intMaxEval = 1000,
                      #max number of evaluations in integration (contLikINT)
                      maxThreads = 16)            # maximum number of threads to be executed by the parallelization 
{
  if (modelDegrad &&
      is.null(kit))
    stop("Please specify a valid kit to model degradation")
  
  #Specify optional model setup:
  kit0 = kit #store kit input
  
  if (!modelDegrad)
    kit = NULL #degradation model turned off when kit=NULL
  
  xi <- xiFW <- 0 #default is no stutters
  
  if (modelBWstutt)
    xi = NULL #empty for now
  
  if (modelFWstutt)
    xiFW = NULL
  
  numRefs = length(refs) #get number of refs
  if (numRefs == 0)
    stop("No references provided. Please specify a valid reference file.")
  
  isEPG = TRUE
  
  condInd = NULL
  
  #Set up hypothesis (contributors)
  cond = rep(0, numRefs) #init condition vector (used to decide which to condition on) - numRefs is ??
  if (!is.null(condInd))
    #null by default
    cond[condInd] = 1:length(condInd) #set contributors (conditional index)
  condhp <- condhd <- cond #copy variable
  condhp[POIind] = max(condhd) + 1 #add POI contributor
  knownRefhd = POIind #known (non-contributor) reference under Hd
  
  if (sum(condhp > 0) > NOC)
    ## of conditional references cannot be larger than # of contributors
    stop(
      "The number of contributors were less than the number of conditionals. Please increase the number of contributors."
    )
  
  #Prepare data
  dat = prepareData(
    samples,
    refs,
    popFreq,
    threshT = threshT,
    minF = minF,
    normalize = FALSE
  ) #obtain data to use for analysis -> prepareData.R function
  
  rmp = calcRMPfst(dat, POIind, condInd, fst = fst) #non-scaled values
  LRupper = -sum(log10(rmp)) #maximum attainable LR (log10 scale) 
  
  #Running through all configurations (uses contLikMLE/validMLEmodel functions):
  modelDegrad = unique(c(FALSE, modelDegrad))
  modelBWstutt = unique(c(FALSE, modelBWstutt))
  modelFWstutt = unique(c(FALSE, modelFWstutt))
  minNOC = sum(condhp > 0)
  searchList = contLikSearch(
    NOC = minNOC:NOC,
    modelDegrad,
    modelBWstutt,
    modelFWstutt,
    samples = dat$samples,
    popFreq = dat$popFreq,
    refData = dat$refData,
    condOrder = condhd,
    knownRefPOI = POIind,
    prC = prC,
    threshT = threshT,
    fst = fst,
    lambda = lambda,
    kit = kit0,
    nDone = nDone,
    seed = seed,
    verbose = FALSE,
    maxThreads = maxThreads,
    alpha = alpha
  )
  
  table <- searchList$outtable
  table[which(is.na(table[,4])),2] <- NA
  adjAIC = table[, 2] #obtain crietion
  optimInd = which.max(adjAIC)[1] #get index of optimal model. Use simpler model if "Tie"
  #set to optimal model:
  mlehp = searchList$hpfitList[[optimInd]]
  mlehd = searchList$hdfitList[[optimInd]]
  
  #####   Checking if correct model is selected #########
  ################################################################################
  
  checkdf <-
    cbind(searchList$modoutcome, table)
  modelSel <- checkdf[which.max(checkdf[, 6]),]
  
  thetahp <- c(mlehp$fit$thetahat)
  thetahd <- c(mlehd$fit$thetahat)
  
  checknC <-
    unique(c(
      NOC == modelSel[, 1],
      NOC == mlehp$prepareC$nC,
      NOC == mlehd$prepareC$nC
    ))
  if (length(checknC) == 2) {
    checknC <- FALSE
  }
  checkdeg <- modelSel[, 2] == FALSE
  checkXi <- modelSel[, 3] == FALSE
  checkXiFW <- modelSel[, 4] == FALSE
  modelcheck <-
    c(checknC, checkdeg, checkXi, checkXiFW)
  
  ####################################################################################
  
  validhp = validMLEmodel(
    mlehp,
    kit0,
    plottitle = "Hp",
    maxThreads = maxThreads,
    alpha = alpha,
    createplot = FALSE,
    verbose = FALSE
  )
  
  validhd = validMLEmodel(
    mlehd,
    kit0,
    plottitle = "Hd",
    maxThreads = maxThreads,
    alpha = alpha,
    createplot = FALSE,
    verbose = FALSE
  )
  
  #####   Sort validity data #########
  ################################################################################
  
  if (any(validhp[, 8])) {
    sighp <-
      paste0(as.character(validhp[which(validhp[, 8]), c(2, 4:7)]), collapse = "/")
  } else {
    sighp <- NA
  }
  
  if (any(validhd[, 8])) {
    sighd <-
      paste0(as.character(validhd[which(validhd[, 8]), c(2, 4:7)]), collapse = "/")
  } else {
    sighd <- NA
  }
  
  ####################################################################################
  
  hpv = logLiki(mlefit = mlehp,
                verbose = FALSE,
                maxThreads = maxThreads)
  hdv = logLiki(mlefit = mlehd,
                verbose = FALSE,
                maxThreads = maxThreads)
  
  ############ model check ################
  ############################################################################################
  
  samejointhp <-
    all.equal(sum(hpv), mlehp$fit$loglik) #check that its same as joint loglik
  samejointhd <-
    all.equal(sum(hdv), mlehd$fit$loglik) #check that its same as joint loglik
  modelcheck <-
    c(modelcheck, samejointhp, samejointhd)
  names(modelcheck) <-
    c(names(modelSel)[1:4], "JointHp", "JointHd")
  
  valmodel <- c(mlehp$prepareC$nC, mlehp$fit$thetahat[4:6], mlehp$fit$loglik, mlehd$fit$loglik)
  names(valmodel) <- names(modelcheck)
  valmodel[is.na(valmodel)] <- 0
  
  if (any(modelcheck == FALSE)) {
    modelflag <-
      paste0(c(names(which(
        modelcheck == FALSE
      )), valmodel[which(modelcheck == FALSE)]), collapse = "/")
  } else {
    modelflag <- NA
  }
  
  ########################################################################################
  
  LRmle = (mlehp$fit$loglik - mlehd$fit$loglik) / log(10) #obtain LR based on mle (log10 scale)
  LRmarker = (hpv - hdv) / log(10) #obtain LR per marker  (log10 scale)
  
  #Perform DC:
  DChd = deconvolve(mlefit = mlehd, verbose = FALSE)
  
  #Perform MCMC:
  mcmchp = contLikMCMC(
    mlehp,
    niter = mcmcIter,
    delta = mcmcDelta,
    seed = seed,
    verbose = FALSE,
    maxThreads = maxThreads
  )
  
  mcmchd = contLikMCMC(
    mlehd,
    niter = mcmcIter,
    delta = mcmcDelta,
    seed = seed + 999,
    verbose = FALSE,
    maxThreads = maxThreads
  )
  
  acceptmcmc <-
    paste0(c(as.character(mcmchp$accrat), as.character(mcmchd$accrat)), collapse = "/")
  
  LRdistr = (mcmchp$postlogL - mcmchd$postlogL) / log(10) #get LR distribution
  LRmarg = (mcmchp$logmargL - mcmchd$logmargL) / log(10)
  LRcons = as.numeric(quantile(LRdistr, 0.05)) #extract 5% quantile  (conservative LR)
  
  #Marginalised likelihood estimation (optimize)
  LRlaplace = (mlehp$fit$logmargL - mlehd$fit$logmargL) / log(10) #obtain LR estimate based on Laplace approx
  
  return(list(
    modelTheta = list(thetahp = thetahp,
                      thetahd = thetahd),
    LRs = list(
      upperlimit = LRupper,
      MLE = LRmle,
      consMCMC = LRcons,
      intlaplace = LRlaplace,
      marginal = LRmarg,
      marker = LRmarker
    ),
    validModel = list(
      flags = modelflag,
      sighp = sighp,
      sighd = sighd,
      acceptrates = acceptmcmc
    ),
    DCrank = DChd$table3
  ))
}

#
#  run_replicate
# 
#  * NOC, popFreq, mx, mu, sigma, kit
#
#  * returns results vector (to be stored in data.frame)
#
run_replicate <- function(NOC = 2, #automaticly use 2 contributors unless otherwise specified
                          popFreq, 
                          mx, 
                          mu, 
                          sigma, 
                          kit,
                          seed = NULL,
                          maxThreads = 32,
                          ThreshT = 50) {  
  # need each replicate to have its own seed, otherwise
  # the set.seed() inside EFM causes some really annoying
  # over-run of randomization
  if(!is.null(seed)) {
    set.seed(seed)
  }
  
  profile <- peaksim(NOC,
                     popFreq,
                     mx,
                     mu,
                     sigma)
  
  res <- simplerun(profile$mix,
                   profile$refs, 
                   popFreq, 
                   NOC,
                   kit,
                   POIind = 2,    # this must be the smallest of the contributors
                   maxThreads = maxThreads,
                   threshT = ThreshT) 
                   
  
  ##################################
  #### Deconvolution Comparison ####
  ##################################
  
  nonmix <- profile$non_mix
  DCdf <- as.data.frame(res$DCrank)
  locs <- names(popFreq)
  
  DCcontr <- NULL
  
  for (n in 1:NOC) {
    contr <- nonmix[which(nonmix[, 2] == n),]
    rownames(contr) <- nonmix[which(nonmix[, 2] == n), 1]
    
    DC <- DCdf[which(DCdf[, 1] == paste0("C", n, sep = "")),] # get contributor
    rownames(DC) <- NULL
    
    if (nrow(DC) == 0) { #could also use dim()
      # DC will be empty if NOC = 1 for EFM 
      #print("data.frame is empty")
      
      DCempty <- data.frame(    #create empty dataframe
        matrix(nrow = (length(locs)),
               ncol = (ncol(DC) + 8))
        )
      colnames(DCempty) <- c( # assign column names (same as if DC contains data)
        colnames(DC[, 1:4]),
        "Rank",
        "Ratio2Next",
        "TrueGenotype",
        "PeakHeights",
        "Allele1",
        "Allele2",
        "TrueAllele1",
        "TrueAllele2"
      )
      
      DCempty[, 1] <- paste0("C", n, sep = "")
      
      DCempty[, 2] <- contr[, 1] 
      
      DCempty[, 5] <- 'Contributor not predicted in "best" model'
      
      # save data from unidentified profile in TrueGenotype, PeakHeights
      DCempty[, 7] <- paste(contr[, 3], contr[, 5], sep = "/")
      DCempty[, 8] <- paste(contr[, 4], contr[, 6], sep = "/")
      
      
      contr[, c(7, 8)] <- contr[, c(3, 5)]
      # assign "drop-out" alleles
      contr[which(contr[, 4] <= ThreshT), 7] <- 99 
      contr[which(contr[, 6] <= ThreshT), 8] <- 99
      
      # save TrueAllele 1 and 2 (includes drop-out)
      DCempty[, c(11, 12)] <- contr[, c(7, 8)]
      
      DCcontr <- rbind(DCcontr, DCempty)
      
    } else{
      #print("data.frame contains data")
      
      
      for (loc in locs) {
        # probabilities are ordered & assigned rank
        
        DC[which(DC[, 2] == loc), 5] <-
          1:length(DC[which(DC[, 2] == loc), 3]) # rank all predicted genotypes
        
        if (length(DC[which(DC[, 2] == loc), 3]) > 1) { # if there is more than one predicted genotype
          for (i in 1:(length(DC[which(DC[, 2] == loc), 3]))) {
            DC[which(DC[, 2] == loc), 6][i] <- # save ratio of marginal probabilities between genotypes
              as.numeric(DC[which(DC[, 2] == loc), 4][i]) / as.numeric(DC[which(DC[, 2] == loc), 4][i + 1]) # this line may cause some errors #fixed!
          }
        } else {
          DC[which(DC[, 2] == loc), 6] <- NA # create empty column for ratio to next genotype
        }
        
      }
      
      contr[, c(7, 8)] <- contr[, c(3, 5)]
      # assign "drop-out" alleles
      contr[which(contr[, 4] <= ThreshT), 7] <- 99 
      contr[which(contr[, 6] <= ThreshT), 8] <- 99
      
      for (i in 1:nrow(DC)) {
        gen <- DC[i, 3]
        match <-
          regmatches(gen, gregexpr("[[:digit:]]+\\.*[[:digit:]]*", gen)) #extract numbers from collapsed proposed genotypes from EFM
        
        propa <- prettyNum(sort(as.numeric(match[[1]]))) # sort and save proposed alleles
        
        truea <-
          prettyNum(sort(as.numeric(contr[which(DC[i, 2] == rownames(contr)), c(7, 8)]))) # extract, sort and save true alleles (includes drop-out alleles)
        
        tgen <-
          paste0(prettyNum(as.numeric(contr[which(DC[i, 2] == rownames(contr)), c(3, 5)])) , collapse = "/") # extract & save true genotype from contributor
        tph <-
          paste0(prettyNum(as.numeric(contr[which(DC[i, 2] == rownames(contr)), c(4, 6)])) , collapse = "/") # extract & save true peak heights from contributor
        
        DC[i, 7:12] <- c(tgen, tph, propa, truea)
      }
      
      colnames(DC) <-
        c(
          colnames(DC[, 1:4]),
          "Rank",
          "Ratio2Next",
          "TrueGenotype",
          "PeakHeights",
          "Allele1",
          "Allele2",
          "TrueAllele1",
          "TrueAllele2"
        )
      DCcontr <- rbind(DCcontr, DC) #combine all contributors into one dataframe
    }
  }
  
  DCrank <- NULL
  
  for (n in 1:NOC) {
    DCtmp <- DCcontr[which(DCcontr[, 1] == paste0("C", n, sep = "")),]
    for (loc in  locs) {
      tmp <- DCtmp[which(DCtmp[, 2] == loc),] # get all rows for specific marker
      
      # check true & drop out alleles against predicted alleles
      if (length(which(tmp[, 9] == tmp[, 11] &
                       tmp[, 10] == tmp[, 12])) == 0) { # if no rows match between true and predicted
        DC <- tmp[1, c(1:2, 4:8)]
        DC[, 3:5] <- c(0, NA, NA) # assign NA rank for true genotypes that were not predicted
      } else {
        DC <-
          tmp[which(tmp[, 9] == tmp[, 11] &
                      tmp[, 10] == tmp[, 12]), c(1:2, 4:8)] # save row with matching true & predicted genotypes, discard others
      }
      DCrank <- rbind(DCrank, DC) # save each row
    }
  }
  rownames(DCrank) <- paste0(DCrank[, 1], sep = ".", DCrank[, 2])
  
  #########################################
  ############ Save Replicate  ############
  #########################################
  results <- as.data.frame(
    c(
      res$modelTheta$thetahp[1:3],
      res$modelTheta$thetahd[1:3],
      res$LRs[1:5],
      res$validModel,
      as.vector(DCrank[, 6]),
      as.vector(DCrank[, 7]),
      as.vector(DCrank[, 4])
    )
  )

  
  cols <-
    c(
      paste(rep(c(
        "ModelHp", "ModelHd"
      ), each = 3), sep = ".",
      rep(c(
        "Mix.Prop", "PH.Expect", "PH.Var"
      ), 2)),
      "UpperLR",
      "MLE.LR",
      "Cons.LR",
      "LaPlaceLR",
      "MargLR",
      "ModelFlags",
      "SigAlleleHp",
      "SigAlleleHd",
      "AcceptRates",
      paste("Genotype", rep(1:NOC, each = length(locs)),
            sep = ".", rep(locs, NOC)),
      paste("PH", rep(1:NOC, each = length(locs)),
            sep = ".", rep(locs, NOC)),
      paste("Rank", rep(1:NOC, each = length(locs)),
            sep = ".", rep(locs, NOC))
    )
  
  cols <-
    c(paste(rep(c(
        "ModelHp", "ModelHd"
      ), each = 3), sep = ".",
      rep(c(
        "Mix.Prop", "PH.Expect", "PH.Var"
      ), 2)),
      "UpperLR",
      "MLE.LR",
      "Cons.LR",
      "LaPlaceLR",
      "MargLR",
      "ModelFlags",
      "SigAlleleHp",
      "SigAlleleHd",
      "AcceptRates",
      paste("Genotype", rep(1:NOC, each = length(locs)),
            sep = ".", rep(locs, NOC)),
      paste("PH", rep(1:NOC, each = length(locs)),
            sep = ".", rep(locs, NOC)),
      paste("Rank", rep(1:NOC, each = length(locs)),
            sep = ".", rep(locs, NOC))
    )
      
      colnames(results) <- cols
      
      return(results)
}
