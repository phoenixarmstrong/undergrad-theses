#!/bin/bash
# Here you should provide the sbatch arguments to be used in all jobs in this serial farm
# It has to contain the runtime switch (either -t or --time):
#SBATCH -t 0-02:59
#SBATCH --mem=256M
#  You have to replace Your_account_name below with the name of your account:
#SBATCH --account=rrg-shaferab      # replace this with your own account
#SBATCH --cpus-per-task=32       # ask for 32 cores per task to let the MCMC run
#SBATCH --mail-user=phoenixarmstrong@trentu.ca # Send email updates to you or someone else
#SBATCH --mail-type=ALL          # send an email in all cases (job started, job ended, job aborted)

module load gcc/9.3.0 r/4.0.2
export R_LIBS=~/R/
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

# Don't change this line:
task.run
