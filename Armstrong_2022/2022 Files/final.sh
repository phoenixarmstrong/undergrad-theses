#!/bin/sh
#SBATCH -t 0-00:10
#SBATCH --mem=4G
#  You have to replace Your_account_name below with the name of your account:
#SBATCH --account=rrg-shaferab      # replace this with your own account
#SBATCH --mail-user=phoenixarmstrong@trentu.ca # Send email updates to you or someone else
#SBATCH --mail-type=ALL          # send an email in all cases (job started, job ended, job aborted)


MU=$mu
SIGMA=$sig
# Only for two-person mixtures
MX=$mx

module load gcc/9.3.0 r/4.0.2
export R_LIBS=~/R/

Rscript --no-save --no-restore ~/Clean_Up.R $MU $SIGMA $MX