#!/bin/bash
#SBATCH -t 0-00:30
#SBATCH --mem=4G
#  You have to replace Your_account_name below with the name of your account:
#SBATCH -A rrg-shaferab
#SBATCH --cpus-per-task=32       # ask for 32 cores per task to let the MCMC run

# Don't change anything below this line

autojob.run
