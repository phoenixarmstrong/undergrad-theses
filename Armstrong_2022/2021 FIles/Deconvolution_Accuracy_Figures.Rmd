---
title: "Deconvolution Accuracy Plots"
author: "Phoenix"
date: "2/8/2022"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
# From Schultze T, Rakotoarisoa A, Schulz-Hardt S. Effects of distance between initial estimates and advice on advice utilization. Judgment and Decision Making, Vol. 10, No. 2, March 2015, pp. 144–171
# https://journal.sjdm.org/14/141112a/
summarySE <- function(data=NULL, measurevar, groupvars=NULL, na.rm=FALSE,
                      conf.interval=.95, .drop=TRUE) {
    library(plyr)

    # New version of length which can handle NA's: if na.rm==T, don't count them
    length2 <- function (x, na.rm=FALSE) {
        if (na.rm) sum(!is.na(x))
        else       length(x)
    }

    # This does the summary. For each group's data frame, return a vector with
    # N, mean, and sd
    datac <- ddply(data, groupvars, .drop=.drop,
      .fun = function(xx, col) {
        c(N    = length2(xx[[col]], na.rm=na.rm),
          mean = mean   (xx[[col]], na.rm=na.rm),
          sd   = sd     (xx[[col]], na.rm=na.rm)
        )
      },
      measurevar
    )

    # Rename the "mean" column    
    datac <- plyr::rename(datac, c("mean" = measurevar))

    datac$se <- datac$sd / sqrt(datac$N)  # Calculate standard error of the mean

    # Confidence interval multiplier for standard error
    # Calculate t-statistic for confidence interval: 
    # e.g., if conf.interval is .95, use .975 (above/below), and use df=N-1
    ciMult <- qt(conf.interval/2 + .5, datac$N-1)
    datac$ci <- datac$se * ciMult

    return(datac)
}
```


# Plotting over each Parameter
Plotting only rank 1
```{r}
library(ggplot2)
library(plyr)
library(dplyr)

all_Rank1 <- all_mu[which(all_mu$Rank==1 ),]

mu <- summarySE(all_Rank1, measurevar="Prop", groupvars=c("Contributor", "Mu")) # get 95% confidence interval

ggplot(aes(x = Mu, y = Prop, fill = Contributor), data = mu) +
    stat_summary(fun=mean, geom="bar", position = "dodge") +
    scale_y_continuous(labels=scales::percent) +
    geom_errorbar(aes(ymin=Prop-ci, ymax=Prop+ci), width=100, position=position_dodge(425)) +
    geom_point(position=position_dodge(425), colour="black",pch=21, size=2) +
    xlab(expression("Peak Height Expectation ("~italic("\u03BC")~")")) +
    ylab("Success Proportion")

mx <- summarySE(all_Rank1, measurevar="Prop", groupvars=c("Contributor", "Mx")) # get 95% confidence interval

ggplot(aes(x = Mx, y = Prop, fill = Contributor), data = mx) +
    stat_summary(fun=mean, geom="bar", position = "dodge") +
    scale_y_continuous(labels=scales::percent) +
    geom_errorbar(aes(ymin=Prop-ci, ymax=Prop+ci), width=0.02, position=position_dodge(0.045)) +
    geom_point(position=position_dodge(0.045), colour="black",pch=21, size=2) +
    xlab( expression("Mixture Proportion ("~italic("\u03D5"["major"])~")" )) + 
    ylab("Success Proportion") 

sig <- summarySE(all_Rank1, measurevar="Prop", groupvars=c("Contributor", "Sigma")) # get 95% confidence interval

ggplot(aes(x = Sigma, y = Prop, fill = Contributor), data = sig) +
    stat_summary(fun=mean, geom="bar", position = "dodge") +
    scale_y_continuous(labels=scales::percent) +
    geom_errorbar(aes(ymin=Prop-ci, ymax=Prop+ci), width=0.02, position=position_dodge(0.09)) +
    geom_point(position=position_dodge(0.09), colour="black",pch=21, size=2) +
    xlab(expression("Coefficent of Variance ("~italic("\u03C3")~")" )) + 
    ylab("Success Proportion")

```
Plotting according to ranks 1-5
```{r}
all_Rank <- all_mu[- c(which(all_mu$Rank=="6" | all_mu$Rank=="7" | all_mu$Rank=="8" | all_mu$Rank=="9" | all_mu$Rank=="10" | all_mu$Rank=="11" | all_mu$Rank=="Error",)),]

pal <- c("#E69F00", "#56B4E9", "#009E73", "#0072B2", "#D55E00", "#CC79A7")

ggplot(data = all_Rank, aes(y = Prop, x = Mu, fill = Rank, colour = Rank))  + 
  scale_colour_manual(values=pal) +
  scale_fill_manual(values=pal) +
  stat_smooth() +
  scale_y_continuous(labels=scales::percent) +
    xlab(expression("Peak Height Expectation ("~italic("\u03BC")~")")) +
    ylab("Success Proportion")

ggplot(data = all_Rank, aes(y = Count/Total, x = Mx, fill = Rank, colour = Rank))  + 
  scale_colour_manual(values=pal) +
  scale_fill_manual(values=pal) +
  stat_smooth() +
  scale_y_continuous(labels=scales::percent) +
    xlab( expression("Mixture Proportion ("~italic("\u03D5"["major"])~")" )) + 
    ylab("Success Proportion") 

ggplot(data = all_Rank, aes(y = Count/Total, x = Sigma, fill = Rank, colour = Rank))  + 
  scale_colour_manual(values=pal) +
  scale_fill_manual(values=pal) +
  stat_smooth() +
  scale_y_continuous(labels=scales::percent) +
    xlab(expression("Coefficent of Variance ("~italic("\u03C3")~")" )) + 
    ylab("Success Proportion") 
```

# Linear Model

With summarized data

```{r}
mod <- glm(Prop ~ poly(Hd_Mx,2) + Hd_Sigma + Contributor , data = all_Rank1, family = "quasibinomial") #try new with Hd_Mx minor (1-Hd_Mx)

summary(mod)

```

$$
\log\left( \frac{rr}{1-rr} \right) = 2.40890 + 11.72025 \phi_{i} - 4.84931 \phi_{i}^2 - 3.90789 \sigma- 1.20061 * \text{Minor}
$$

```{r} 
int = 2.40890
mix = 11.72025
mix2 = -4.84931
sig = -3.90789
con <- -1.20061

#tests with quasi binomial
#test 1 no degredation
b <- exp(int + mix * 0.5 + mix2 * 0.5 + sig * 0.63 + con * 1) #minor
#rr/(1-rr)  = b
rr <- b/(b+1)
rr

b <- exp(int + mix * 0.5 + mix2 * 0.5 + sig * 0.63 + con * 0) #major
#rr/(1-rr)  = b
rr <- b/(b+1)
rr


#test2
b <- exp(int + mix * 0.27 + mix2 * 0.27 + sig * 0.4 + con * 1) #minor
#rr/(1-rr)  = b
rr <- b/(b+1)
rr

b <- exp(int + mix * 0.73 + mix2 * 0.73 + sig * 0.4 + con * 0) #major
#rr/(1-rr)  = b
rr <- b/(b+1)
rr
```



# Summary Statistics
```{r}
summary(all_Rank1)
summary(all_Rank1[which(all_Rank1$Contributor == "Major"),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor"),5])

#### Error ####

Err <- all_mu[all_mu$Rank == "Error",]

sum(Err$Count)/(16*1000*nrow(Err)) # overall error-out rate in EFM processing
nrow(Err)/2

#### Mu ####


summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mu == 500),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mu == 500),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mu == 1000),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mu == 1000),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mu == 1500),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mu == 1500),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mu == 2000),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mu == 2000),5])

summary(all_Rank[which(all_Rank$Rank == "2"),5])

#### Sigma ####

summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Sigma == 0.1),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Sigma == 0.1),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Sigma == 0.2),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Sigma == 0.2),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Sigma == 0.3),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Sigma == 0.3),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Sigma == 0.4),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Sigma == 0.4),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Sigma == 0.5),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Sigma == 0.5),5])


summary(all_Rank[which(all_Rank$Rank == "NA" & all_Rank$Sigma == 0.1),5])
summary(all_Rank[which(all_Rank$Rank == "2" & all_Rank$Sigma == 0.1),5])
summary(all_Rank[which(all_Rank$Rank == "2" & all_Rank$Sigma == 0.5),5])


#### Mx ####

summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mx == 0.95),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mx == 0.90),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mx == 0.80),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mx == 0.70),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mx == 0.60),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mx == 0.55),5])


summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mx == 0.95),5])

summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mx == 0.90),5])

summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mx == 0.80),5])

summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mx == 0.70),5])

summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mx == 0.60),5])

summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mx == 0.55),5])


summary(all_Rank1[which(all_Rank1$Contributor == "Major" & all_Rank1$Mx >= 0.70),5])
summary(all_Rank1[which(all_Rank1$Contributor == "Minor" & all_Rank1$Mx >= 0.70),5])


summary(all_Rank[which(all_Rank$Rank == "2" & all_Rank$Mx == 0.55),5])
summary(all_Rank[which(all_Rank$Rank == "3" & all_Rank$Mx == 0.55),5])
summary(all_Rank[which(all_Rank$Rank == "NA" & all_Rank$Mx == 0.55),5])
summary(all_Rank[which(all_Rank$Rank == "2" & all_Rank$Mx == 0.95),5])


```

