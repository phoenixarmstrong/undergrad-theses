#!/bin/bash
#SBATCH --account=rrg-shaferab      # replace this with your own account
#SBATCH --nodes=25               # number of nodes, each acts like a "computer"
#SBATCH --cpus-per-task=32       # ask for 32 cores per task to let the MCMC run
#SBATCH --mem-per-cpu=256M      # memory; default unit is megabytes
#SBATCH --time=0-02:59           # time (DD-HH:MM)
#SBATCH --mail-user=phoenixarmstrong@trentu.ca # Send email updates to you or someone else
#SBATCH --mail-type=ALL          # send an email in all cases (job started, job ended, job aborted)

module load gcc/9.3.0 r/4.0.2
export R_LIBS=~/R/
export NODESLIST=$(echo $(srun hostname | cut -f 1 -d '.'))
R CMD BATCH --no-save --no-restore Run_Sim_tryCatch_servers.R
