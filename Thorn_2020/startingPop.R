
# returns a random starting population based on MAF it is given.
#returns data.frame
startingPop <- function(start_size = 10,
                        maf,
                        total.loci){
  
  stopifnot(class(maf) == "numeric", is.vector(maf))
  
  if(length(maf) == 1) {
    maf <- rep(maf, total.loci)
  } else if(length(maf) > 1 & length(maf) != total.loci) {
    stop("Argument maf must be either 1 number or total.loci numbers.")
  } else if(!all(maf < 1) | !all(maf > 0)) {
    stop("Argument maf must contain only numbers in [0, 1].")
  }
  
  n <- round(maf * start_size, 0)

  pop <- matrix(rep(0, 2 * start_size * total.loci), 
                nrow = total.loci, byrow = TRUE)
  
  for(loci in 1:total.loci) {
    
    homol <- sample(1:(2 * start_size), n[loci], replace = FALSE)
    pop[loci, homol] <- rep(1, length(homol))
    
  }
  pop
}

