source("findcRMP.R")
source("popSimulation.R")
source("recombineFunction.R")
source("findFreqs.R")
options(scipen=999)

library(shiny)
library(ggplot2)
library(hypred)

ui <- fluidPage(
  # App title ----
  titlePanel("Random Match Probability Simulation"),
  
  plotOutput('plot'),
  
  hr(),
  
    
    fluidRow(
      column(5,
             numericInput(inputId = "gen",
                          label = "Length, in years, species has been isolated:",
                          value = 100,
                          min = 1),
             numericInput(inputId = "g",
                          label = "Species generation time, in years:",
                          value = 10,
                          min = 1)
                          
             ),
      column(3,
          numericInput(inputId = "SNP",
                       label = "Number of SNPs:",
                       value = 50,
                       min= 1, max = 1000),
          numericInput(inputId = "N",
                       label = "Population Size:",
                       value = 100,
                       min = 10,
                       max = 1000000)
      ),
      column(3,
          numericInput(inputId = "samples",
                       label = "How Many Samples",
                       value = 100,
                       max = 1000, min = 1),
          br(),
          actionButton("go", "Go")
      )
    
    )
  
)

server <- function(input, output){
  
  p <- eventReactive(input$go,{
                G <- round(input$gen * 1/input$g, 0)
                #cant have decimal generations, so round to the nearest 1
                N <- input$N
                SNP <- input$SNP
  class(input$N)
  genome <- hypred::hypredGenome(1, 3.5, SNP)
  population <- popSimulation(G, N, 1e-9, genome)
  results <- data.frame(x = 1:input$samples,
                        rmp = 1:input$samples)
  freq <- population[[2]][ ,G]
  
  for(i in 1:input$samples){
    indiv <- sample(1:N, 1)
    
    homo1 <- (2*indiv)-1
    homo2 <- 2*indiv
    
    randomPerson <- population[[1]][ ,c(homo1, homo2)]
    
    results[i, 2] <- -log10(findcRMP(freq, randomPerson))
    
    }
  
  ggplot(data = results, aes(x = x, y = rmp, ymin = 0)) +
    geom_point() +
    xlab("sample") +
    ylab("-log RMP") +
    geom_point(data = results, aes(x = input$samples/2, y = mean(rmp), color = "red", size = 2.5)) +
    theme(legend.position = "none") +
    geom_text(x = input$samples/2, y = mean(results$rmp)-1.5, label = "Mean RMP", color = "red") +
    geom_hline(yintercept = -log10(1/100000000000000), linetype="dashed", size = 0.2) +
    geom_text(x=3.5, y=13, label="1 in 100 trillion", size = 4)
  
  })
  
  
  output$plot <- renderPlot({
    p()
  })  
  
  
}

shinyApp(ui = ui, server = server)

